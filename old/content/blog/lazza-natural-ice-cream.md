---
title: "Lazza Natural Ice Creams Added to our products"
date: 2019-10-31T16:09:44+05:30
draft: false
image: "https://lh3.googleusercontent.com/gHiSb_Co6WsF4YqCloIjMlk0v-5owyzeh4WxG3MitL1c1CDuq8-xpMDQ-54nJp1wsta2xX3_r_JOWSAK=w600-h0"
---
We are excited to announce that we have added **Lazza Natural Icecreams** to our product list.
Please come & enjoy the refreshing taste !
